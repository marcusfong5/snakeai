class GameScene extends Phaser.Scene {
    constructor () {
        super({key: 'GameScene'});
    }

    create() {
        let U = 0;
        let D = 0;
        let L = 0;
        let R = 0;

        gameState.dList = ['up', 'down', 'left', 'right']; //move direction
        gameState.snake = [this.add.rectangle(135, 95, 10, 10, 0x2d2d2), this.add.rectangle(125, 95, 10, 10, 0x2d2d2)];
        gameState.score = this.add.text(130, 450, 'Snake by Marcus Fong');
        gameState.apple = this.add.rectangle(Math.floor(Math.random()*config.width/10)*10+5, Math.floor(Math.random()*config.height/10)*10+5, 10, 10, 0xff0606);

        gameState.U = this.add.text(140, 10, `U: 30`, { fontSize: '10px' });
        gameState.D = this.add.text(140, 20, `D: 30`, { fontSize: '10px' });
        gameState.L = this.add.text(140, 30, `L: 30`, { fontSize: '10px' });
        gameState.R = this.add.text(140, 40, `R: 30`, { fontSize: '10px' });


        gameState.snake.Dir = gameState.dList[0]; //initial direction is up



        const moveSnake = () => {
            for (let i = gameState.snake.length-1; i > 0; i--) {
                gameState.snake[i].x = gameState.snake[i-1].x;
                gameState.snake[i].y = gameState.snake[i-1].y;
            }

            if (gameState.snake.Dir === 'up') {
                gameState.snake[0].y -= 10; //up
            } else if (gameState.snake.Dir === 'down') {
                gameState.snake[0].y += 10; //down
            } else if (gameState.snake.Dir === 'left') {
                gameState.snake[0].x -= 10; //left
            } else if (gameState.snake.Dir === 'right') {
                gameState.snake[0].x += 10; //right
            }
        }

        const hitEdge = () => {
            if (gameState.snake[0].x < 0 || gameState.snake[0].x > config.width
                || gameState.snake[0].y < 0 || gameState.snake[0].y > config.height)
                this.scene.restart();
        }

        const logSnake = () => {
            let snakeLocations = []
            for (let i = 0; i < gameState.snake.length; i++) {
                snakeLocations.push([gameState.snake[i].x, gameState.snake[i].y]);
            }
            console.log(snakeLocations);
            return snakeLocations;
        }

        const allLocations = () => {
            let locList = []
            for (let i = 1; i <= 20; i++) {
                for (let j = 1; j <= 20; j++) {
                    locList.push([i*10-5,j*10-5])
                }
            }
            return locList;
        }


        const eatApple = () => {
            if(gameState.apple.x === gameState.snake[0].x && gameState.apple.y === gameState.snake[0].y) {
                //snake to grow by one
                if(gameState.snake[gameState.snake.length-1].x === gameState.snake[gameState.snake.length-2].x)
                    if(gameState.snake[gameState.snake.length-1].y > gameState.snake[gameState.snake.length-2].y)
                        gameState.snake.push(this.add.rectangle(gameState.snake[gameState.snake.length-1].x, gameState.snake[gameState.snake.length-1].y + 10, 10, 10, 0x2d2d2))//push one block below
                    else if(gameState.snake[gameState.snake.length-1].y < gameState.snake[gameState.snake.length-2].y)
                        gameState.snake.push(this.add.rectangle(gameState.snake[gameState.snake.length-1].x, gameState.snake[gameState.snake.length-1].y - 10, 10, 10, 0x2d2d2))//push one block up
                    else if(gameState.snake[gameState.snake.length-1].y === gameState.snake[gameState.snake.length-2].y)
                        if(gameState.snake[gameState.snake.length-1].x > gameState.snake[gameState.snake.length-2].x)
                            gameState.snake.push(this.add.rectangle(gameState.snake[gameState.snake.length-1].x + 10, gameState.snake[gameState.snake.length-1].y, 10, 10, 0x2d2d2))//push one block right
                        else if(gameState.snake[gameState.snake.length-1].x < gameState.snake[gameState.snake.length-2].x)
                            gameState.snake.push(this.add.rectangle(gameState.snake[gameState.snake.length-1].x - 10, gameState.snake[gameState.snake.length-1].y, 10, 10, 0x2d2d2))//push one block left

                //apple disappear
                //apple regenerate in new location
                // let temp = allLocations().filter(x => !logSnake().includes(x))
                // const rand = Math.random();
                //
                // gameState.apple.x = temp[Math.floor(rand * temp.length)][0];
                // gameState.apple.y = temp[Math.floor(rand * temp.length)][1];

                gameState.apple.x = Math.floor(Math.random()*config.width/10)*10+5;
                gameState.apple.y = Math.floor(Math.random()*config.height/10)*10+5;
            }
        }



        const hitTail = () => {
            for (let i = 1; i < gameState.snake.length; i++) {
                if (gameState.snake[0].x === gameState.snake[i].x && gameState.snake[0].y === gameState.snake[i].y) {
                    this.scene.restart();
                }
            }
        }


        ////////////////////////////////////////////////////////////////////////////////

        // (negative) distance from snake head to apple
        function distance(head_x, head_y, apple_x, apple_y) {
            return Math.sqrt(Math.pow((head_x - apple_x),2) + Math.pow((head_y - apple_y),2));
        }

        //normalised distance parameter
        function distVar(head_x, head_y, apple_x, apple_y) {
            if (distance(head_x, head_y, apple_x, apple_y) === 0) {
                return 1.0;
            } else {
                return 1/distance(head_x, head_y, apple_x, apple_y);
            }
        }

        //Heuristic score formula
        function Hscore(head_x, head_y, apple_x, apple_y, body) {

            //Avoid tail collision
            for (let i = 1; i < body.length; i++) {
                if (head_x === body[i].x && head_y === body[i].y) {
                    return 0;
                }
            }

            //Avoid edge collision
            if (head_x < 0 || head_x > config.width
                || head_y < 0 || head_y > config.height)
                return 0;

            // Pick best distance
            else
                return distVar(head_x, head_y, apple_x, apple_y);
        }


        //minimaxAI
        function minimaxAI() {

            const go = []; //Define movement list

            //Assign heuristic for each possible move
            const goLeft = [Hscore(gameState.snake[0].x - 10, gameState.snake[0].y, gameState.apple.x, gameState.apple.y,gameState.snake),'left'];
            const goRight = [Hscore(gameState.snake[0].x + 10, gameState.snake[0].y, gameState.apple.x, gameState.apple.y,gameState.snake),'right'];
            const goUp = [Hscore(gameState.snake[0].x, gameState.snake[0].y - 10, gameState.apple.x, gameState.apple.y,gameState.snake),'up'];
            const goDown = [Hscore(gameState.snake[0].x, gameState.snake[0].y + 10, gameState.apple.x, gameState.apple.y,gameState.snake),'down'];

            //Push heuristics into a list
            go.push(goUp); //0th item
            go.push(goDown); //1st item
            go.push(goLeft); //2nd item
            go.push(goRight); //3rd item

            //Show heuristics on screen
            gameState.U.setText(`U: ${go[0][0]}`)
            gameState.D.setText(`D: ${go[1][0]}`)
            gameState.L.setText(`L: ${go[2][0]}`)
            gameState.R.setText(`R: ${go[3][0]}`)

            //Define best move
            const bestMove = go.sort()[3][1]; //best move
            const nxtBestMove = go.sort()[2][1]; //alt move if best move is illegal

            //Make best move
            if (bestMove === 'left') {
                if (gameState.snake.Dir !== 'right')
                    gameState.snake.Dir = bestMove;
                else {
                    gameState.snake.Dir = nxtBestMove;
                }
            }
            if (bestMove === 'right') {
                if (gameState.snake.Dir !== 'left')
                    gameState.snake.Dir = bestMove;
                else {
                    gameState.snake.Dir = nxtBestMove;
                }
            }
            if (bestMove === 'up') {
                if (gameState.snake.Dir !== 'down')
                    gameState.snake.Dir = bestMove;
                else {
                    gameState.snake.Dir = nxtBestMove;
                }
            }
            if (bestMove === 'down') {
                if (gameState.snake.Dir !== 'up')
                    gameState.snake.Dir = bestMove;
                else {
                    gameState.snake.Dir = nxtBestMove;
                }
            }
        }

        const gameLoop = () => {
            hitTail(); // restart if snake hits tail
            hitEdge(); // restart if snake hits edge
            logSnake();
            minimaxAI(); //Snake AI
            moveSnake(); // function to move snake given direction
            eatApple(); //make snake longer if eats apple
        }

            //Controls
            this.input.keyboard.on('keyup_UP', function (e) {
                if (gameState.snake.Dir !== gameState.dList[1])
                    gameState.snake.Dir = gameState.dList[0];
            }, this);
            this.input.keyboard.on('keyup_DOWN', function (e) {
                if (gameState.snake.Dir !== gameState.dList[0])
                    gameState.snake.Dir = gameState.dList[1];
            }, this);
            this.input.keyboard.on('keyup_LEFT', function (e) {
                if (gameState.snake.Dir !== gameState.dList[3])
                    gameState.snake.Dir = gameState.dList[2];
            }, this);
            this.input.keyboard.on('keyup_RIGHT', function (e) {
                if (gameState.snake.Dir !== gameState.dList[2])
                    gameState.snake.Dir = gameState.dList[3];
            }, this);

            // Loop every 200 ms
            const snakeMoveLoop = this.time.addEvent({
                delay: 50,
                callback: gameLoop,
                callbackScope: this,
                loop: true,
            });
        }
}
