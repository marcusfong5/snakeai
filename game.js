const gameState = {
};

const config = {
    type: Phaser.AUTO,
    width: 200,
    height: 200,
    backgroundColor: 0x000000,
    scene: [ GameScene ],
    physics: {
        default: 'arcade',
        arcade: {
            gravity: {y: 200},
            enableBody: true
        }
    }
};

const game = new Phaser.Game(config);